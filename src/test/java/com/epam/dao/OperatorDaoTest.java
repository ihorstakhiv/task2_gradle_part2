package com.epam.dao;

import org.junit.jupiter.api.Test;
import java.sql.SQLException;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OperatorDaoTest {

    @Test
    public void detectLifeProviderTest() {
        ArrayList<String> life = new ArrayList<>();
        life.add("0631111111");
        life.add("0731111111");
        life.add("0931111111");
        life.forEach(s -> assertTrue(new OperatorDao().detectNetworkProvider(s) == 2));
    }

    @Test
    public void detectKyivstarProviderTest() throws SQLException {
        ArrayList<String> kyivstar = new ArrayList<>();
        kyivstar.add("0671111111");
        kyivstar.add("0961111111");
        kyivstar.add("0971111111");
        kyivstar.add("0681111111");
        kyivstar.forEach(s -> assertTrue(new OperatorDao().detectNetworkProvider(s) == 1));
    }
}