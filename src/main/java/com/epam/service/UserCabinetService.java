package com.epam.service;

import com.epam.dao.UserCabinetDao;
import com.epam.model.User;
import java.sql.SQLException;

public class UserCabinetService {
    public double getBalance(User user) {
        return new UserCabinetDao().getBalance(user);
    }

    public int refillBalance(User user, double amountToAdd) {
        return new UserCabinetDao().refillBalance(user, amountToAdd);
    }

    public int updateBalance(User user, double newBalance) {
        return new UserCabinetDao().updateBalance(user, newBalance);
    }

    public int deductBalance(User user, double amoutToDeduct) {
        return new UserCabinetDao().minusBalance(user, amoutToDeduct);
    }

    public int getPhoneID(User user) throws SQLException {
        return new UserCabinetDao().getPhoneID(user);
    }
}
