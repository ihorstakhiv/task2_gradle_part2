package com.epam.service;

import com.epam.dao.OperatorDao;
import com.epam.model.Operator;
import com.epam.model.User;
import java.sql.SQLException;

public class OperatorService {
    public Operator getNetworkProvider(User user) throws SQLException {
        return new OperatorDao().getNetworkProvider(user);
    }

    public int detectNetworkProvider(String number) {
        return new OperatorDao().detectNetworkProvider(number);
    }
}
