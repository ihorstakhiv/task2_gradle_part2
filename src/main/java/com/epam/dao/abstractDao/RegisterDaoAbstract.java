package com.epam.dao.abstractDao;

public interface RegisterDaoAbstract {
    int createUser(String name, String surname, String username, String password);
    int createPhone(String name, String surname, String username, String password, String model, String number, int networkProviderId);
    int getUserID(String name, String surname, String username, String password);
}
