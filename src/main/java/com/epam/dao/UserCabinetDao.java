package com.epam.dao;

import com.epam.dao.abstractDao.UserCabinetDaoAbstract;
import com.epam.database.DatabaseConnector;
import com.epam.model.User;
import com.epam.utils.PropertiesReader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserCabinetDao implements UserCabinetDaoAbstract {

    @Override
    public double getBalance(User user) {
        Connection connection = DatabaseConnector.getDBConnection();
        ResultSet rs = null;
        double balance = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(PropertiesReader.getStringFromProperties("GET_BALANCE"));
            ps.setInt(1, user.getId());
            rs = ps.executeQuery();
            while (rs.next()) {
                balance = rs.getDouble("balance");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return balance;
    }

    @Override
    public int refillBalance(User user, double amountToAdd) {
        int rowsUpdated = 0;
        ResultSet rs = null;
        Connection connection = DatabaseConnector.getDBConnection();
        rowsUpdated = getRowsUpdated(user, getBalance(user) + amountToAdd, rowsUpdated, connection);
        return rowsUpdated;
    }

    @Override
    public int updateBalance(User user, double newBalance) {
        int rowsUpdated = 0;
        ResultSet rs = null;
        Connection connection = DatabaseConnector.getDBConnection();
        rowsUpdated = getRowsUpdated(user, newBalance, rowsUpdated, connection);
        return rowsUpdated;
    }

    private int getRowsUpdated(User user, double newBalance, int rowsUpdated, Connection connection) {
        try {
            PreparedStatement ps = connection.prepareStatement(PropertiesReader.getStringFromProperties("UPDATE_BALANCE"));
            ps.setDouble(1, newBalance);
            ps.setInt(2, getPhoneID(user));
            rowsUpdated = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsUpdated;
    }

    @Override
    public int minusBalance(User user, double amoutToDeduct) {
        double finalBalance = 0;
        if ((getBalance(user) - amoutToDeduct) < 0) {
            finalBalance = 0.0;
        } else {
            finalBalance = getBalance(user) - amoutToDeduct;
        }
        int rowsUpdated = 0;
        ResultSet rs = null;
        Connection connection = DatabaseConnector.getDBConnection();
        rowsUpdated = getRowsUpdated(user, finalBalance, rowsUpdated, connection);
        return rowsUpdated;
    }

    @Override
    public int getPhoneID(User user) throws SQLException {
        int phoneID = 0;
        ResultSet rs = null;
        Connection connection = DatabaseConnector.getDBConnection();
        try {
            PreparedStatement ps = connection.prepareStatement(PropertiesReader.getStringFromProperties("GET_PHONE_ID"));
            ps.setInt(1, user.getId());
            rs = ps.executeQuery();
            while (rs.next()) {
                phoneID = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return phoneID;
    }
}
