package com.epam.view.menu;

import com.epam.controller.CabinetMenuController;
import com.epam.controller.Controller;
import com.epam.utils.constant.MenuConstant;
import com.epam.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class UserMenu {

    public static final Logger logger = LogManager.getLogger(UserMenu.class);
    private Map<Integer, String> cabinetMenu;
    private Map<Integer, Controller> cabinetMethodMenu;
    private User user;

    public UserMenu(User user) {
        this.user = user;
        cabinetMenu = new HashMap<>();
        cabinetMethodMenu = new HashMap<>();

        cabinetMenu.put(1, "All about  you and your SIM");
        cabinetMenu.put(2, "Show balance");
        cabinetMenu.put(3, "Call");
        cabinetMenu.put(4, "Write Message");
        cabinetMenu.put(5, "Add money to your balance");
        cabinetMenu.put(6, "Call history");
        cabinetMenu.put(7, "Message history");
        cabinetMenu.put(8, "Exit");

        cabinetMethodMenu.put(1, new CabinetMenuController(user)::getAccountInfo);
        cabinetMethodMenu.put(2, new CabinetMenuController(user)::showBalance);
        cabinetMethodMenu.put(3, new CabinetMenuController(user)::makeCall);
        cabinetMethodMenu.put(4, new CabinetMenuController(user)::sendMessage);
        cabinetMethodMenu.put(5, new CabinetMenuController(user)::addMoneyToBalance);
        cabinetMethodMenu.put(6, new CabinetMenuController(user)::getCallHistory);
        cabinetMethodMenu.put(7, new CabinetMenuController(user)::getMessageHistory);
        cabinetMethodMenu.put(8, new CabinetMenuController(user)::exit);
    }

    public void outputSubCabinetMenu() {
        Scanner scanner = new Scanner(System.in);
        int key = MenuConstant.getKey();
        do {
            print();
            logger.info("Please make your choice: ");
            try {
                key = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                logger.error(e.toString());
                continue;
            }
            if (!this.cabinetMethodMenu.containsKey(key)) {
                logger.error("Invalid choose !" + "\nPlease try again : ");
                continue;
            }
            this.cabinetMethodMenu.get(key).print();
        } while (key != MenuConstant.getMaxUserMenuValue());
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void print() {
        cabinetMenu.forEach((k, v) -> logger.info(k + " - " + v));
    }
}
