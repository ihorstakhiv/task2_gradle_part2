package com.epam.view.menu;


import com.epam.controller.Controller;
import com.epam.controller.WelcomeMenuController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.HashMap;
import java.util.Map;

public class WelcomeMenu {

    public static final Logger logger = LogManager.getLogger(WelcomeMenu.class);
    private Map<Integer, String> welcomeMenu;
    public Map<Integer, Controller> welcomeMethodMenu;

    public WelcomeMenu() {
        welcomeMenu = new HashMap<>();
        welcomeMethodMenu = new HashMap<>();

        welcomeMenu.put(1, " Login ");
        welcomeMenu.put(2, " Registration ");
        welcomeMenu.put(3, " Exit ");

        welcomeMethodMenu.put(1, new LoginMenu()::outputSubLoginMenu);
        welcomeMethodMenu.put(2, new RegistrationMenu()::outputSubRegisterMenu);
        welcomeMethodMenu.put(3, new WelcomeMenuController()::quit);
    }

    public void print() {
        logger.info(" Welcome to Mobile Service =) " + "\n");
        welcomeMenu.forEach((k, v) -> logger.info(k + " - " + v));
    }
}
