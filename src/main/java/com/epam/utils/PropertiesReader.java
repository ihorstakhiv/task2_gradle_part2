package com.epam.utils;

import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public class PropertiesReader {
    public static String getStringFromProperties(String name) {
        Properties properties = new Properties();
        try {
            properties.load(Objects.requireNonNull(PropertiesReader.class.getClassLoader().getResourceAsStream("sqlrequest.properties")));
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(name);
    }
}
