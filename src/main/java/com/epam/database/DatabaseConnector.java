package com.epam.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnector {

    public static Logger logger = LogManager.getLogger(DatabaseConnector.class);
    private static final String URL = "jdbc:mysql://localhost:3306/networking_DB?serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true";
    private static final String USER ="root";
    private static final String PASSWORD ="root";
    private static Connection connection = null;

    public static Connection getDBConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(URL, USER, PASSWORD);
                if (connection.isValid(100)) {
                    logger.trace("DB connection is set up successfully.\n");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
}
