package com.epam.controller;

import com.epam.controller.Controller;
import com.epam.model.User;
import com.epam.service.LoginService;
import com.epam.view.menu.UserMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Scanner;

public class LoginMenuController implements com.epam.controller.Controller {
    public static final Logger logger = LogManager.getLogger(LoginMenuController.class);

    public void provideUserCredentials() {
        Scanner scanner = new Scanner(System.in);
        logger.info("Enter username:");
        String username = scanner.nextLine();
        logger.info("Enter password:");
        String password = scanner.nextLine();
        try {
            User user = new LoginService().getUser(username, password);
            if ((!Objects.isNull(user)) && user.getUsername().equals(username) && user.getPassword().equals(password)) {
                logger.debug("Hello, " + user.getName() + " " + user.getSurname());
                new UserMenu(user).outputSubCabinetMenu();
            } else {
                logger.error("User not found\n");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void back() {
        logger.trace("Back to com.epam.Application menu.\n");
    }

    @Override
    public void print() {
    }
}
